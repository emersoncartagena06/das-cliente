﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RestSharp;
using System.Threading.Tasks;

namespace ConsumirWebApi.Controllers
{

    public class Persona
    {
        public string nombre { get; set; }
        public string apellido { get; set; }
    }
    public class SaludarController : Controller
    {
        //GET: Saludar
        public async Task<IActionResult> Index()
        {
            Persona p = new Persona() { nombre = "Emerson", apellido = "Cartagena" };
            string mensaje = "";
            var client = new RestClient("https://localhost:44329");
            var request = new RestRequest("HolaMundo", Method.Post);
            //request.AddHeader("postman-token", "4378fac6-5877-22f2-51f6-2a82eec00ea2");
            //request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddJsonBody(p);
            RestResponse response = await client.ExecuteAsync(request);

            if (response.IsSuccessful)
            {
                mensaje = JsonConvert.DeserializeObject<string>(response.Content);
            }
            else
            {
                mensaje = "No se pudo obtener información";
            }

            return View("Index", mensaje);
        }
    }
}
